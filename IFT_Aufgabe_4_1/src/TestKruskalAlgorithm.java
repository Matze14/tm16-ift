import graph.Vertex;
import graph.WeightedGraph;
import graph.WeightedGraphImpl;
import graph.algorithm.MinSpanTreeKruskalAlgorithm;

/**
 * Test class for Kruskal's algorithm 
 *
 * @author Mas Dallüge & Matthias Graffenberger
 * @version 2.5 2016-10-19
 */
public class TestKruskalAlgorithm {
	

	public static void main(String[] args) throws Exception {
		
		WeightedGraphImpl graph = new WeightedGraphImpl(false);
		Vertex  v1, v2, v3, v4, v5, v6;

		// Beispielgraph aus der Vorlesung
		v1 = new Vertex("1");
		v2 = new Vertex("2");
		v3 = new Vertex("3");
		v4 = new Vertex("4");
		v5 = new Vertex("5");
		v6 = new Vertex("6");
		
		// Erstelle die Kanten zu den Knoten mit Kosten
		graph.addEdge(v1, v2, 2.0);
		graph.addEdge(v1, v6, 1.0);
		graph.addEdge(v2, v6, 1.0);
		graph.addEdge(v3, v5, 2.0);
		graph.addEdge(v5, v6, 2.0);
		graph.addEdge(v3, v6, 2.0);
		graph.addEdge(v4, v5, 4.0);
		graph.addEdge(v4, v6, 5.0);
		graph.addEdge(v3, v4, 6.0);
		graph.addEdge(v2, v3, 7.0);
		
		//System.out.println("Gewichteter Graph: "+graph.toString());
		
		MinSpanTreeKruskalAlgorithm algorithm = new MinSpanTreeKruskalAlgorithm(graph);
		WeightedGraph minimumSpanningTree = algorithm.minimumSpanningTree();
		
		System.out.println("Minimal aufspannender Baum: "+minimumSpanningTree.toString());
	}
}
