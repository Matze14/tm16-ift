package pushpuzzle;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Diese Klasse startet die Applikation und
 * stellt ein Hauptmenü zur Verfügung.
 * 
 * @author Matthias Graffenberger, Max Dallüge
 *
 */
public class MainMenu extends Application {

	/**
	 * Die Main-Methode startet die Applikation.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Application.launch(args);
	}

	/**
	 * Diese Methode wird beim Start der Applikation ausgeführt.
	 * Hier wird der Layoutmanager und die benötigten grafischen
	 * Elemente geladen.
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		// setzen des titels
		primaryStage.setTitle("Hauptmenü");
		// setzen eines icons
		primaryStage.getIcons().add(new Image("TH.png"));

		// Generelle Beschreibung
		Label description = new Label("Bitte geben Sie die Höhe und\n Breite des Spielfeldes an.");
		description.setPadding(new Insets(5.00));

		// Beschreibung für Höheneingabefeld
		Label heightDescription = new Label("Höhe: ");
		heightDescription.setAlignment(Pos.CENTER_RIGHT);
		heightDescription.setPrefWidth(100.0);

		// Beschreibung für Breiteneingabefeld
		Label widthDescription = new Label("Breite: ");
		widthDescription.setAlignment(Pos.CENTER_RIGHT);
		widthDescription.setPrefWidth(100.0);

		// Textfeld für die Höheneingabe
		TextField height = new TextField();
		height.setPadding(new Insets(5.00));
		height.setPrefWidth(250.0);

		// Textfeld für die Breiteneingabe
		TextField width = new TextField();
		width.setPadding(new Insets(5.00));
		width.setPrefWidth(250.0);

		// Hier wird der Bestätigungsbutton initialisiert
		Button createField = new Button("Spielfeld generieren");
		
		// gesonderte Einstellungen für den Button
		createField.setPadding(new Insets(5.00));
		createField.setPrefWidth(250.0);
		createField.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				/*
				 * Wenn auf den Button geklickt wird, wird
				 * ein Gamboard-Objekt erstellt, welches die
				 * GUI für die Eingabe des Startspielstandes 
				 * ermöglicht. 
				 */
				Gameboard gameboard = new Gameboard();
				int heightValue = Integer.parseInt(height.getText());
				int widthValue = Integer.parseInt(width.getText());
				
				//primaryStage wird benötigt um das Fenster des Hauptmenüs zu verwenden.
				gameboard.createNewGameboard(primaryStage, heightValue, widthValue);
			}
		});

		// Hier wird ein Layout-Manager erstellt
		GridPane gridPane = new GridPane();
		GridPane.setMargin(description, new Insets(10.0));
		GridPane.setMargin(heightDescription, new Insets(10.0, 0.0, 10.0, 10.0));
		GridPane.setMargin(widthDescription, new Insets(10.0, 0.0, 10.0, 10.0));
		GridPane.setMargin(height, new Insets(10.0, 10.0, 10.0, 0.0));
		GridPane.setMargin(width, new Insets(10.0, 10.0, 10.0, 0.0));
		GridPane.setMargin(createField, new Insets(10.0));

		// hier wird der rasterbasierte Layout-Manager befüllt
		gridPane.add(description, 1, 0);
		gridPane.add(heightDescription, 0, 1);
		gridPane.add(height, 1, 1);
		gridPane.add(widthDescription, 0, 2);
		gridPane.add(width, 1, 2);
		gridPane.add(createField, 1, 3);
		
		// die scene beinhaltet alle Objekte die in der Stage dargestellt werden sollen
		Scene scene = new Scene(gridPane);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
