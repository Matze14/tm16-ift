package pushpuzzle;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Diese Klasse stellt die GUI bereit, in der
 * alle Eingaben getätigt werden könne, um den 
 * Startzustand des spieles darzustellen.
 * 
 * @author Matthias Graffenberger, Max Dallüge
 *
 */
public class Gameboard {

	// Variablendeklaration
	private ArrayList<ArrayList<TextField>> gameBoard = null;
	private VBox vBox = null;

	/**
	 * In diesem Kunstruktor wird ein mehrdimensionales Array
	 * erstellt, welches grafische JavaFX-Komponenten beinhaltet
	 * um das Spielfeld dynamisch zu generieren.
	 */
	public Gameboard() {
		this.gameBoard = new ArrayList<ArrayList<TextField>>();
		this.vBox = new VBox();
	}

	/**
	 * In dieser Methode wird das Spielfeld dynamisch generiert.
	 * 
	 * @param stage "Fenster" des Hauptmenüs
	 * @param height gewünschte Höhe des Spielfeldes
	 * @param width gewünschte Breite des Spielfeldes
	 */
	public void createNewGameboard(Stage stage, int height, int width) {
		/*
		 * Für die Darstellung der Eingabemöglichkeiten wird wieder ein
		 * rasterbasierter Layout-Manager verwendet.
		 */
		
		GridPane gridPane = new GridPane();
		
		// ein Beschreibungslabel, damit der Nutzer weiß was er tun muss
		Label description = new Label("Bitte entfernen Sie aus einem beliebeigen Feld\nden "+
										"Haken, um das Spielfeld korrekt zu bestücken.");
		
		
		/*
		 * Hier wird passend der Höhen- und Breitenangabe
		 * ein Feld aus Textfelder generiert in die später
		 * die Nummern der Puzzle-Teile eingetragen werden
		 * können.
		 */
		for (int i = 0; i < height; i++) {
			// neue Zeile hinzufügen
			this.gameBoard.add(new ArrayList<TextField>());
			for (int j = 0; j < width; j++) {
				TextField field = new TextField();
				//einheitliche Größe
				field.setPrefWidth(30.0);;
				
				this.gameBoard.get(i).add(field);
				gridPane.add(field, j, i);
				
				GridPane.setMargin(field, new Insets(15.0));
			}
		}
		
		// ein Button um das befüllte Rätsel zu lösen
		Button searchButton = new Button("Rätsel lösen!");
		searchButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				// hier soll die Berechnung gestartet werden
				System.out.println("Berechnung startet...");
				// die Klasse PushpuzzleSolver beinhaltet die Lösungslogik
				PushpuzzleSolver pushpuzzleSolver = new PushpuzzleSolver(gameBoard, height, width);
				pushpuzzleSolver.solvePuzzle();
			}
		});
		
		/*
		 * Als übergeordneter Layout-Manager wurde eine vBox gewählt,
		 * welcher zeilenweise arbeitet. Diese entscheidung wurde aus
		 * rein optischen Gründen gewählt.
		 */
		this.vBox.getChildren().add(description);
		this.vBox.getChildren().add(gridPane);
		this.vBox.getChildren().add(searchButton);
		
		VBox.setMargin(description, new Insets(15.0));
		VBox.setMargin(searchButton, new Insets(15.0));
		
		// hier werden die grafischen Darstellungskomponenten übergeben
		Scene scene = new Scene(this.vBox);
		stage.setScene(scene);
		stage.show();
	}
}
