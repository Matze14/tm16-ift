package pushpuzzle;


import java.util.ArrayList;

import javafx.scene.control.TextField;

/**
 * Diese Klasse beinhaltet die Logik, um das
 * Pushpuzzle mittels Tiefensuche zu lösen.
 * @author Matthias Graffenberger, Max Dallüge
 *
 */
public class PushpuzzleSolver {

	// Variablendeklaration
	private ArrayList<ArrayList<TextField>> gameboardToSolve = null;
	private int[][] transformedGameboard = null;

	/**
	 * In diesem Konstruktor wird das Gameboard übergeben und die Höhe und
	 * Breite des Pushpuzzles.
	 * 
	 * @param gameboard
	 * @param height
	 * @param width
	 */
	public PushpuzzleSolver(ArrayList<ArrayList<TextField>> gameboard, int height, int width) {
		this.gameboardToSolve = gameboard;
		this.transformedGameboard = new int[height][width];
	}

	/**
	 * In dieser Methode soll das Puzzle gelöst werden.
	 * Hier werden ausgelagerte Methoden aufgerufen, um eine
	 * Lösung zu erzielen.
	 */
	public void solvePuzzle() {
		/*
		 * In diesem Schritt wird die mehrdimensionale ArrayList
		 * aus Textfeldern in ein mehrdimensionales Array aus Integer-Werten
		 * überführt um besser mit den Daten arbeiten zu können.
		 */
		for (int i = 0; i < this.gameboardToSolve.size(); i++) {
			ArrayList<TextField> row = this.gameboardToSolve.get(i);
			for (int j = 0; j < row.size(); j++) {
				try {
					int nextValue = Integer.parseInt(row.get(j).getText());
					this.transformedGameboard[i][j] = nextValue;
				} catch (Exception e) {
					/*
					 * Sollte eine ungültige Eingabe erfolgen, löst die Methode
					 * Integer.parseInt(s); eine Exception aus, welche hier aufgefangen
					 * wird. In dem Fall eines leeren oder ungültigen Strings wird
					 * eine Null an die entsprechende Stelle geschrieben.
					 */
					this.transformedGameboard[i][j] = 0;
				}
			}
		}
		// hier wird das aktuelle Spielfeld als String ausgegeben.
		this.printActualSolvingStep();
		// hier wird geprüft, ob das Rätsel bereits gelöst wurde
		System.out.println("Ist das Rätsel gelöst: "+this.isFinalState());
		// dieser Schritt soll den nächsten Lösungsschritt durchführen
		this.makeTheNextStep();
	}
	
	/**
	 * In dieser Methode sollen die einzelnen Lösungsschritte durchgeführt werden.
	 * Leider wurde diese Funktion nicht vollständig implementiert und ist somit
	 * nicht funktionsfähig.
	 */
	private void makeTheNextStep(){
		//Folgende Richtungen gibt es:
		//nach oben
		//nach unten
		//nach rechts
		//nach links
		for (int i = 0; i < this.transformedGameboard.length; i++) {
			for (int j = 0; j < this.transformedGameboard[i].length; j++) {
				if(this.transformedGameboard[i][j] == 0){
					/*
					 * Hier wurde nach der leeren Stelle gesucht. Als nächster
					 * Schritt würde nach möglichen Zügen gesucht werden. Diese
					 * müssten abgespeichert werden. Danach muss ein Zug gewählt 
					 * werden um in der Suche fortzufahren.
					 */
					System.out.println("Hier ist das freie Feld!");
				}
			}
		}
	}

	/**
	 * In dieser Methode wird geprüft ob das Rätsel gelöst wurde
	 * und die Steine in der richtigen Reihenfolge vorhanden sind.
	 * 
	 * @return "true" für ja und "false" für nein
	 */
	private boolean isFinalState() {
		int lengthOfProovingList = this.transformedGameboard.length*this.transformedGameboard[0].length;
		int listCounter = 0;
		boolean isFinalState = true;
		int[] proovingList = new int[lengthOfProovingList];
		
		/*
		 * An dieser Stelle wird das mehrdimensionale Array in ein
		 * eindimensionales Array gewandelt, um die Überprüfung 
		 * leichter durchzuführen.
		 */
		for (int i = 0; i < this.transformedGameboard.length; i++) {
			for (int j = 0; j < this.transformedGameboard[i].length; j++) {
				proovingList[listCounter] = this.transformedGameboard[i][j];
				listCounter++;
			}
		}
		
		/*
		 * Als nächstes wird überprüft ob die Liste aufsteigend sortiert ist
		 * Ist dies der Fall, liefert die Methode den Wert "true" zurück. Ist
		 * dies allerdings nicht der Fall wird ein "false" zurück gelifert.
		 */
		for (int i = 0; i < proovingList.length-1; i++) {
			int valueA = proovingList[i];
			int valueB = proovingList[i+1];
			//System.out.println("("+valueA+")");
			if(valueB == 0){
				//System.out.println("Eine Null!");
			} else if(valueA > valueB){
				//System.out.println("Wert ("+valueA+") ist größer als Wert ("+valueB+")!");
				isFinalState = false;
			} 
		}
		return isFinalState;
	}

	/**
	 * Diese Methdoe gibt das aktuelle Feld aus.
	 */
	private void printActualSolvingStep() {
		// das mehrdimensionale Array wird iterativ ausgegeben
		for (int i = 0; i < this.transformedGameboard.length; i++) {
			for (int j = 0; j < this.transformedGameboard[i].length; j++) {
				System.out.printf("%d\t", this.transformedGameboard[i][j]);
			}
			System.out.println("\n");
		}
	}
}
