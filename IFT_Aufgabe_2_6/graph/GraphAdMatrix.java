package graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
/**
 * @author Matthias Graffenberger, Florian Schütze und Max Dallüge
 * @date 5.10.2016
 * @brief Diese Klasse ist eine Implementierung der Schnittstelle Graph.
 * Hier wurden NICHT alle Funktionen implementiert, da es laut Aufgabe
 * 2.6 nicht nötig war. 
 */
public class GraphAdMatrix implements Graph {

	//Variablendeklaration
	private static final long serialVersionUID = -7457786364472167655L;
	private ArrayList<ArrayList<Integer>> g_adjazenzmatrix = null;
	private ArrayList<Vertex> g_listOfVertices = null;
	private boolean g_isDirected = false;

	public GraphAdMatrix(boolean isDirected) {
		this.g_adjazenzmatrix = new ArrayList<ArrayList<Integer>>();
		this.g_listOfVertices = new ArrayList<Vertex>();
		this.g_isDirected = isDirected;
	}

	/**
	 * @brief Diese Methode fügt eine neue Ecke zur Adjazenzmatrix hinzu.
	 */
	@Override
	public void add(Vertex v) throws Exception {
		// Vertex zu der Liste hinzufügen
		this.g_listOfVertices.add(v);

		if (this.g_adjazenzmatrix != null) {
			// wenn ArrayList nicht null ist
			if (this.g_adjazenzmatrix.isEmpty()) {
				/*
				 * Wenn die ArrayList leer ist, wird eine neue angelegt und der
				 * Stelle 0 der Wert 0 hinzugefügt. Wenn der Wert 0 ist, gibt es
				 * keine Kante.
				 */
				this.g_adjazenzmatrix.add(new ArrayList<Integer>());
				this.g_adjazenzmatrix.get(0).add(0);
			} else {
				/*
				 * Wenn die ArrayList nicht leer ist, wird eine neue angelegt
				 * und der Matrix hinzugefügt. Als nächster Schritt werden in
				 * den vorhandenen ArrayLists die Nullen (keine Verbindungen)
				 * aufgefüllt, und in der neuen ArrayList alle Stellen (länge
				 * des Matrix Arrays) ebenfalls mit Nullen aufgefüllt. Ist so,
				 * da ein neuer Knoten keine Kante mich sich selbst oder anderen
				 * Knoten hat. Diese müssen sebarat hinzugefügt werden.
				 */
				this.g_adjazenzmatrix.add(new ArrayList<Integer>());
				for (ArrayList<Integer> l_arrayList : this.g_adjazenzmatrix) {
					for (int i = l_arrayList.size(); i < this.g_adjazenzmatrix.size(); i++) {
						l_arrayList.add(0);
					}
				}
			}
		}
	}

	/**
	 * @brief Durch den Aufruf dieser Methode wird 
	 * eine neue Kante hinzugefügt.
	 */
	@Override
	public Edge addEdge(Vertex v1, Vertex v2) throws Exception {
		Edge l_edge = new Edge(v1, v2);

		//wenn v1 Teil des Graphen ist
		if (this.g_listOfVertices.contains(v1)) {
			//wenn v2 Teil des Graphen ist
			if (this.g_listOfVertices.contains(v2)) {
				//Ermittlung der Indizes der beiden Ecken
				int l_indexV1 = this.g_listOfVertices.indexOf(v1);
				int l_indexV2 = this.g_listOfVertices.indexOf(v2);
				//prüfen ob der Graph gerichtet ist
				if (this.g_isDirected) {
					/*
					 * Wenn der Graph gerichtet ist, ist die Richtung 
					 * entscheidend, weswegen keine Spiegelung des Eintrags
					 * vorgenommen wird.
					 */
					this.g_adjazenzmatrix.get(l_indexV1).set(l_indexV2, 1);
				} else {
					/*
					 * Wenn der Graph ungerichtet ist, ist die Richtung 
					 * nicht entscheidend, weswegen eine Spiegelung des Eintrags
					 * vorgenommen wird.
					 */
					this.g_adjazenzmatrix.get(l_indexV1).set(l_indexV2, 1);
					this.g_adjazenzmatrix.get(l_indexV2).set(l_indexV1, 1);
				}
			}
		}

		return l_edge;
	}

	/**
	 * @brief Diese Methode liefert den Wert "true" zurück, wenn
	 * zwischen den beiden übergebenen Ecken eine Kante besteht.
	 * Der Wert "false" wird zurückgegeben, falls keine Kante existiert.
	 */
	@Override
	public boolean haveCommonEdge(Vertex v1, Vertex v2) {
		//überprüfen, ob v1 eine Ecke des Graphen ist
		if (this.g_listOfVertices.contains(v1)) {
			//überprüfen, ob v2 eine Ecke des Graphen ist
			if (this.g_listOfVertices.contains(v2)) {
				//die Indizes der Ecken ermitteln
				int l_indexV1 = this.g_listOfVertices.indexOf(v1);
				int l_indexV2 = this.g_listOfVertices.indexOf(v2);

				//prüfen ob es ein gerichteter Graph ist
				if (this.g_isDirected) {
					/*
					 * Wenn es ein gerichteter Graph ist, muss die Reihenfolge der Angabe
					 * der Ecken (Richtung) beachtet werden
					 */
					if (this.g_adjazenzmatrix.get(l_indexV1).get(l_indexV2) == 1) {
						return true;
					} else {
						return false;
					}
				} else {
					/*
					 * Wenn es ein ungerichteter Graph ist, müssen beide Richtungen geprüft werden.
					 */
					if ((this.g_adjazenzmatrix.get(l_indexV1).get(l_indexV2) == 1)
							&& (this.g_adjazenzmatrix.get(l_indexV2).get(l_indexV1) == 1)) {
						return true;
					} else {
						return false;
					}
				}

			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @brief Diese Methode liefert eine Liste aller Ecken, die
	 * mit der übergebenen Ecke durch eine Kante verbunden sind. 
	 * (Nachbarn)
	 */
	@Override
	public List<Vertex> getAdjacentVertices(Vertex v) {
		ArrayList<Vertex> l_resultSet = new ArrayList<Vertex>();
		//es wird geprüft, ob die Ecke im Graphen existiert
		if (this.g_listOfVertices.contains(v)) {
			//es wird der Index der Ecke ermittelt
			int l_indexV = this.g_listOfVertices.indexOf(v);
			
			//die Reihe in der sich alle Kanten der übergeben Ecke befinden
			ArrayList<Integer> l_row = this.g_adjazenzmatrix.get(l_indexV);

			//die Reihe der Adjazenzmatrix wird durchlaufen
			for (int i = 0; i < l_row.size(); i++) {
				//wenn eine Kante besteht, wird die anliegende Ecke hinzugefügt
				if (l_row.get(i) == 1) {
					l_resultSet.add(this.g_listOfVertices.get(i));
				}
			}
			return l_resultSet;
		} else {
			return l_resultSet;
		}
	}

	/**
	 * @brief Diese Methode berechnet den Grad einer übergebenen Ecke.
	 */
	@Override
	public int getDegree(Vertex v) {
		// prüft, ob die Ecke im Graphen vorhanden ist
		if (this.g_listOfVertices.contains(v)) {
			// ermittelt den Index der Ecke
			int l_indexV = this.g_listOfVertices.indexOf(v);
			int l_degreeOfVertex = 0;
			/*
			 * Durchläuft die Reihe der Adjazenzmatrix, in der sich die Kanten
			 * der Ecke befinden.
			 */
			ArrayList<Integer> row = this.g_adjazenzmatrix.get(l_indexV);
			for (int i = 0; i < row.size(); i++) {
				if (row.get(i) == 1) {
					l_degreeOfVertex++;
				}
			}
			return l_degreeOfVertex;
		} else {
			return -1;
		}
	}

	/**
	 * @brief Diese Methode liefert den Graphen als Zeichenkette zurück.
	 */
	public String toString() {
		return "Vertices: " + this.g_listOfVertices.toString() + "\n" + "Edges: " + this.getAllEdges().toString();
	}

	/**
	 * @brief Diese Methode liefert ein HashSet aller Kanten des Graphen.
	 */
	@Override
	public Set<Edge> getAllEdges() {
		HashSet<Edge> l_allEdges = new HashSet<Edge>();
		for (int i = 0; i < this.g_adjazenzmatrix.size(); i++) {
			ArrayList<Integer> row = this.g_adjazenzmatrix.get(i);
			for (int j = 0; j < row.size(); j++) {
				if (row.get(j) == 1) {
					// Kante die hinzugefügt werden soll
					Edge l_nextEdge = new Edge(this.g_listOfVertices.get(i), this.g_listOfVertices.get(j));
					// Gegenteil der Kante, die hinzugefügt werden soll
					// dient nur zum Überprüfen
					Edge l_oppositeEdge = new Edge(this.g_listOfVertices.get(j), this.g_listOfVertices.get(i));

					/*
					 * Hier wird geprüft, ob die neue Kante bereits in
					 * umgekehrter Form in der Liste vorhanden ist. Ist dies der
					 * Fall, wird diese Kante nicht nochmal hinzugefügt.
					 */
					if (!l_allEdges.isEmpty()) {
						// wird "false", wenn eine äquivalente Kante gefunden
						// wurde
						boolean l_noEdgeFound = true;
						// die Liste durchsuchen
						for (Edge nextEdge : l_allEdges) {
							if (nextEdge.toString().equals(l_oppositeEdge.toString())) {
								l_noEdgeFound = false;
								break;
							}
						}
						/*
						 * Wenn keine äquivalente Kante gefunden wurde, soll die
						 * neue Kante hinzugefügt werden.
						 */
						if (l_noEdgeFound) {
							l_allEdges.add(l_nextEdge);
						}
					} else {
						l_allEdges.add(l_nextEdge);
					}
				}
			}
		}
		return l_allEdges;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Die folgenden Methoden müssen für die
	// Aufgabe 2.6 nicht implementiert werden.
	//////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public boolean isDirected() {
		// Muss nicht implementiert werden
		return false;
	}

	@Override
	public int getVerticesCount() {
		// Muss nicht implementiert werden
		return 0;
	}

	@Override
	public void remove(Vertex v) throws Exception {
		// Muss nicht implementiert werden

	}

	@Override
	public Iterator<Vertex> getVerticesIterator() {
		// Muss nicht implementiert werden
		return null;
	}

	@Override
	public List<Vertex> cloneVertices() {
		// Muss nicht implementiert werden
		return null;
	}

	@Override
	public Edge createEdge(Vertex v1, Vertex v2) {
		// Muss nicht implementiert werden
		return null;
	}

	@Override
	public void addEdge(Edge e) throws Exception {
		// Muss nicht implementiert werden

	}

	@Override
	public void removeEdge(Edge e) throws Exception {
		// Muss nicht implementiert werden

	}

	@Override
	public void removeEdges(Vertex v) throws Exception {
		// Muss nicht implementiert werden

	}

	@Override
	public int getDegree() {
		// Muss nicht implementiert werden
		return 0;
	}

	@Override
	public List<Vertex> getVertices() {
		// Muss nicht implementiert werden
		return null;
	}

	@Override
	public Set<Vertex> getVertices(int degree) {
		// Muss nicht implementiert werden
		return null;
	}

	@Override
	public List<Edge> getEdges(Vertex v) {
		// Muss nicht implementiert werden
		return null;
	}

	@Override
	public List<Vertex> getIncomingAdjacentVertices(Vertex v) {
		// Muss nicht implementiert werden
		return null;
	}

	@Override
	public List<Vertex> getOutgoingAdjacentVertices(Vertex v) {
		// Muss nicht implementiert werden
		return null;
	}

	@Override
	public Set<Vertex> getAdjacentVertices(List<Vertex> vertices) {
		// Muss nicht implementiert werden
		return null;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
}
