package userinterface;

import java.util.List;

import graph.GraphAdMatrix;
import graph.Vertex;

/**
 * @author Matthias Graffenberger, Florian Schütze und Max Dallüge
 * @date 5.10.2016
 * @brief Diese Klasse beinhaltet die Main-Methode und startet das Programm.
 * In der Methode werden vordefinierte Tests durchgeführt. 
 */
public class GraphUI {

	public static void main(String[] args) {
		GraphAdMatrix l_graph = new GraphAdMatrix(false);
		try {
			Vertex v1 = new Vertex();
			Vertex v2 = new Vertex();
			Vertex v3 = new Vertex();
			Vertex v4 = new Vertex();
			Vertex v5 = new Vertex();
			Vertex v6 = new Vertex();

			v1.setString("v1");
			v2.setString("v2");
			v3.setString("v3");
			v4.setString("v4");
			v5.setString("v5");
			v6.setString("v6");

			l_graph.add(v1);
			l_graph.add(v2);
			l_graph.add(v3);
			l_graph.add(v4);
			l_graph.add(v5);

			l_graph.addEdge(v3, v2);
			l_graph.addEdge(v4, v5);
			l_graph.addEdge(v5, v3);
			l_graph.addEdge(v5, v2);

			l_graph.add(v6);
			
			// Test 1
			System.out.println("Test 1:");
			System.out.println("-----------------------------------");
			System.out.println("Ausgabe des Graphen:");
			System.out.println(l_graph.toString());
			System.out.println("-----------------------------------");

			// Test 2
			System.out.println("\nTest 2:");
			System.out.println("-----------------------------------");
			System.out.println("Ist eine Kante zwischen v4 und v5 vorhanden: " + l_graph.haveCommonEdge(v4, v5));
			System.out.println("-----------------------------------");

			// Test 3
			System.out.println("\nTest 3:");
			System.out.println("-----------------------------------");
			System.out.println("Die Kanten von v5: ");
			List<Vertex> test = l_graph.getAdjacentVertices(v5);
			for (Vertex vertex : test) {
				System.out.println(vertex.toString());
			}
			System.out.println("-----------------------------------");
			
			// Test 4
			System.out.println("\nTest 4:");
			System.out.println("-----------------------------------");
			System.out.println("Die Ecke v5 hat den folgenden Grad: "+l_graph.getDegree(v5));
			System.out.println("-----------------------------------");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
