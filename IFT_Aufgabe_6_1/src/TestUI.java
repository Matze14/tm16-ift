import graph.GraphException;
import graph.Network;
import graph.NetworkException;
import graph.NetworkImpl;
import graph.Vertex;
import graph.algorithm.AugmentNetworkFlow;

/**
 * Diese Klasse stellt die Testklasse dar. Hier werden die Aufgabenstellungen
 * bearbeitet.
 * 
 * @author Matthias Graffenberger und Max Dallüge
 */
public class TestUI {

	public static void main(String[] args) {
		System.out.println("Erstelle Graphen...");
		Network network = new NetworkImpl();
		try {
			// Alle nötigen Ecken erstellen
			Vertex vSource = new Vertex("q");
			Vertex v1 = new Vertex("1");
			Vertex v2 = new Vertex("2");
			Vertex v3 = new Vertex("3");
			Vertex v4 = new Vertex("4");
			Vertex v5 = new Vertex("5");
			Vertex v6 = new Vertex("6");
			Vertex vSink = new Vertex("s");

			/*
			 * Dem Graphen die Kanten aus dem Beispiel inklusive Kantenbewertung
			 * hinzufügen. Im selben Atemzug werden auch die Vertex-Objekte
			 * erstellt.
			 */
			network.addEdge(vSource, v1, 40.0);
			network.addEdge(vSource, v4, 20.0);
			network.addEdge(v4, v5, 54.0);
			network.addEdge(v1, v2, 30.0);
			network.addEdge(v5, v3, 50.0);
			network.addEdge(v2, v3, 10.0);
			network.addEdge(v2, v6, 37.0);
			network.addEdge(v6, v4, 17.0);
			network.addEdge(v3, vSink, 22.0);
			network.addEdge(v6, vSink, 32.0);

			// Hier werden Quelle und Senke festgelegt
			network.setSink(vSink);
			network.setSource(vSource);
		} catch (Exception e) {
			System.err.println("Das Netzwerk konnte nicht erstellt werden!");
		}

		System.out.println("Berechne den maximalen Fluss...");

		try {
			AugmentNetworkFlow flow = new AugmentNetworkFlow(network);
			// Berechnung des maximalen Flusses
			flow.maximizeFlow();
			
			//Ausgabe der Ergebnisse
			System.out.println("Der maximale Fluss beträgt: " + flow.getTotalFlow());
			System.out.println("Die Anzahl der Erweiterungswege ist: " + flow.getAmmountOfSteps());
		} catch (NetworkException e) {
			System.err.println("Das gegebene Netzwerk konnte nicht verwendet werden!");
			e.printStackTrace();
		} catch (GraphException e) {
			System.err.println("Der Berechnung ist fehlgeschlagen!");
		}
	}
}
