package graph.algorithm;

import graph.*;
import java.util.*;
import java.io.*;

/**
 * Abstract class for handling a flow on a <tt>Network</tt>.
 *
 * Concrete implementations of this class must never modify the Network itself.
 *
 * @author Ralf Vandenhouten
 * @version 2.0 2010-09-15
 */
public abstract class NetworkFlow implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The <tt>Network</tt> object on which the flow will be defined.
	 */
	Network network;

	/**
	 * Source and sink of the network.
	 */
	Vertex source, sink;

	/**
	 * The HashMap for mapping the (directed) edges of the <tt>Network</tt> to
	 * their flows.
	 */
	HashMap<Edge,Number> flowMap;

	/**
	 * Constructor to be used by subclasses of this abstract class.
	 */
	public NetworkFlow( Network network ) throws NetworkException {
		this.network = network;
		this.source = network.getSource();
		this.sink = network.getSink();
		if ( source == null || sink == null )
			throw new NetworkException("Source or sink missing.");
		this.flowMap = new HashMap<Edge,Number>();
	}

	/**
	 * Returns a HashMap that maps each edge of the network to its flow.
	 *
	 * @return The HashMap that maps each edge of the network to its flow.
	 */
	public Map<Edge,Number> getFlowMap() {
		return flowMap;
	}

	/**
	 * Returns the total flow coming from the source and flowing to the sink.
	 *
	 * @return The total flow (absolute value of the flow).
	 */
	public double getTotalFlow() throws GraphException {
		double totalFlow = 0;
		Vertex v = network.getSource();
		if ( v == null )
			throw new NetworkException("No source defined.");

		// Add all outgoing flows to the total flow
		for (Edge e : network.getEdges(v)) {
			try {
				totalFlow += flowMap.get(e).doubleValue();
			} catch (Exception ex) {
				throw new NetworkException("Incomplete flow definition.");
			}
		}
		return totalFlow;
	}

	/**
	 * Initializes the flow in the network by setting the flow of each edge to 0.
	 */
	public void initializeFlow() {
		for (Edge edge : network.getAllEdges())
			flowMap.put(edge, new Double(0));
	}

	/**
	 * Method for finding a maximum flow in the network.
	 */
	abstract public void maximizeFlow();
}