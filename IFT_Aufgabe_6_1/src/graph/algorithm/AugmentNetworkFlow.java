package graph.algorithm;

import java.util.*;
import graph.*;

/**
 * Class that provides a flow on a <tt>Network</tt> and maximizes it by
 * augmentation paths using the algorithm of Edmonds and Karp.
 *
 * @author Ralf Vandenhouten
 * @version 2.1 2016-11-02
 * @Edited Matthias Graffenberger und Max Dallüge
 */
public class AugmentNetworkFlow extends NetworkFlow {

	private static final long serialVersionUID = 2L;

	/**
	 * Number of vertices in the network.
	 */
	int nvertices;

	/**
	 * Indices of source and sink vertex, respectively.
	 */
	int sourceIndex, sinkIndex;

	/**
	 * Array storing the vertices linearly.
	 */
	Vertex[] vertex;

	/**
	 * HashMap storing the indices of the vertices in the above array vertex.
	 */
	HashMap<Vertex, Integer> vertIndex;

	/**
	 * Adjacency matrix storing the capacities of the edges.
	 */
	double[][] capacity;

	/**
	 * Flow matrix storing the flow of the edges.
	 */
	double[][] flow;

	/**
	 * Array for storing the predecessor of each vertex during augmentation
	 * search.
	 */
	int[] predecessor;

	/**
	 * Array for storing the edge type (true=forward, false=backward) for each
	 * edge during augmentation search.
	 */
	boolean[] forwardEdge;

	/**
	 * Diese Variable speichert die Anzahl der Erweiterungswege.
	 */
	private int ammountOfSteps;

	/**
	 * Constructor for instances of this class.
	 */
	public AugmentNetworkFlow(Network network) throws NetworkException {
		super(network);
		int i = 0, j;
		nvertices = network.getVerticesCount();
		vertex = new Vertex[nvertices];
		vertIndex = new HashMap<Vertex, Integer>();
		capacity = new double[nvertices][nvertices];
		flow = new double[nvertices][nvertices];
		predecessor = new int[nvertices];
		forwardEdge = new boolean[nvertices];

		// Setzt die Anzahl der Erweiterungswege auf null
		ammountOfSteps = 0;

		// Initializing the data structures
		for (Iterator<Vertex> it = network.getVerticesIterator(); it.hasNext();) {
			Vertex v = it.next();
			vertIndex.put(v, new Integer(i));
			vertex[i++] = v;
		}
		for (i = 0; i < nvertices; i++) {
			predecessor[i] = -1;
			for (j = 0; j < nvertices; j++) {
				capacity[i][j] = 0.0;
				flow[i][j] = 0.0;
			}
		}
		try {
			sourceIndex = vertIndex.get(source);
			sinkIndex = vertIndex.get(sink);
		} catch (Exception e) {
			throw new NetworkException("Source or sink not defined.");
		}
		// Initialize the capacity matrix
		for (Iterator<Edge> it = network.getAllEdges().iterator(); it.hasNext();) {
			WeightedEdge e = (WeightedEdge) it.next();
			i = vertIndex.get(e.getVertexA());
			j = vertIndex.get(e.getVertexB());
			capacity[i][j] = e.getWeight();
		}
	}

	/**
	 * Method for finding a maximum flow in the network. The method uses the
	 * algorithm of Edmonds and Karp by iteratively searching augmenting paths.
	 */
	public void maximizeFlow() {
		double delta;
		// iterative search for augmentation paths
		do {
			delta = augmentationPath();
			if (delta > 0.0) {
				increaseFlow(delta);
				/*
				 * Hier wird die Anzahl der Erweiterungswege
				 * erhöht.
				 */
				increaseAmmountOfSteps();
			}
		} while (delta > 0.0);

		// Finally generate the flow HashMap
		for (Iterator<Edge> it = network.getAllEdges().iterator(); it.hasNext();) {
			WeightedEdge e = (WeightedEdge) it.next();
			int i = vertIndex.get(e.getVertexA());
			int j = vertIndex.get(e.getVertexB());
			flowMap.put(e, new Double(flow[i][j]));
		}
	}

	/**
	 * Method for increasing the flow after a new augmenting path has been
	 * computed.
	 */
	private void increaseFlow(double delta) {
		int v, pred;
		v = sinkIndex;
		while ((pred = predecessor[v]) != -1) {
			if (forwardEdge[v]) // forward edge
				flow[pred][v] += delta;
			else // backward edge
				flow[v][pred] -= delta;
			v = pred;
		}
	}

	/**
	 * This method searches an augmentation path for the current flow.
	 */
	private double augmentationPath() {
		int i, j;
		// Array for storing the flow change for each vertex
		double[] fDelta = new double[nvertices];
		// Array for marking the visited vertices
		boolean[] visited = new boolean[nvertices];
		// Queue for the breadth-first-search
		Queue<Integer> queue = new ArrayDeque<Integer>();
		// Initialising
		predecessor[sourceIndex] = -1;
		visited[sourceIndex] = true;
		fDelta[sourceIndex] = Double.POSITIVE_INFINITY;
		fDelta[sinkIndex] = 0.0;
		// Search the shortest augmenting path with breadth-first-search
		queue.add(sourceIndex);
		while (fDelta[sinkIndex] == 0.0 && !queue.isEmpty()) {
			try {
				i = queue.remove();
			} catch (NoSuchElementException e) {
				i = 0;
			} // should never happen...
				// handle successors of this vertex
			for (j = 0; j < nvertices; j++)
				if (capacity[i][j] > 0)
					if (!visited[j] && flow[i][j] < capacity[i][j]) {
						visited[j] = true;
						queue.add(j);
						fDelta[j] = Math.min(capacity[i][j] - flow[i][j], fDelta[i]);
						predecessor[j] = i;
						forwardEdge[j] = true;
					}
			// handle predecessors of the vertex
			for (j = 0; j < nvertices; j++)
				if (capacity[j][i] > 0)
					if (!visited[j] && flow[j][i] > 0.0) {
						visited[j] = true;
						queue.add(j);
						fDelta[j] = Math.min(flow[j][i], fDelta[i]);
						predecessor[j] = i;
						forwardEdge[j] = false;
					}
		}
		return fDelta[sinkIndex];
	}

	/**
	 * Diese Methode liefert die Anzahl der
	 * Erweiterungswege.
	 */
	public int getAmmountOfSteps() {
		return ammountOfSteps;
	}
	
	/**
	 * Diese Methode erhöht die Anzahl der
	 * Erweiterungswege bei Aufruf um 1.
	 */
	public void increaseAmmountOfSteps() {
		this.ammountOfSteps++;
	}
}