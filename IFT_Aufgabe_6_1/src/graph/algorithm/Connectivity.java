package graph.algorithm;

import graph.*;
import java.util.*;
import java.io.*;

/**
 * Class that computes the connectivity of a undirected graph.
 *
 * @author Ralf Vandenhouten
 * @version 2.0 2010-09-16
 */
public class Connectivity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The graph considered.
	 */
	Graph graph;

	/**
	 * The vertices of the graph.
	 */
	List<Vertex> vertices;

	/**
	 * The Menger-network generated from the graph.
	 */
	Network network;

	/**
	 * The two Menger sets of vertices in the network, stored as ArrayLists.
	 */
	ArrayList<Vertex> v1, v2;

	/**
	 * Constructor for instances of this class.
	 */
	public Connectivity( Graph graph ) {
		this.graph = graph;
		generateNetwork();
	}

	/**
	 * Computes the (node) connectivity number of the considered graph using
	 * the Menger algorithm (see Turau, Addison-Wesley 1996).
	 *
	 * @return The connectivity number of the graph.
	 */
	public int getConnectivity() throws GraphException {
		int j, i = 0, n = graph.getVerticesCount(), connect = n-1;
		UnitCapacityNetworkFlow ucflow;

		while ( i <= connect ) {
			i++;
			for (j=i+1; j<=n; j++) {
				Vertex v = vertices.get(i-1);
				Vertex w = vertices.get(j-1);
				if ( !graph.haveCommonEdge( v, w ) ) {
					network.setSource( v2.get(i-1) );
					network.setSink( v1.get(j-1) );
					ucflow = new UnitCapacityNetworkFlow( network );
					ucflow.maximizeFlow();
					connect = Math.min( connect, (int)ucflow.getTotalFlow());
				}
			}
		}
		return connect;
	}

	/**
	 * Generates the Menger network out of the given graph.
	 */
	protected void generateNetwork() {
		network = new NetworkImpl();
		int i, j, nvertices = graph.getVerticesCount();
		v1 = new ArrayList<Vertex>(nvertices);
		v2 = new ArrayList<Vertex>(nvertices);
		vertices = graph.getVertices();

		// generate vertices
		for (i=0; i<nvertices; i++) {
			try {
				Vertex v = vertices.get(i);
				Vertex w = new Vertex(v.toString()+"'");
				v1.add(i, w);
				network.add( w );
				w = new Vertex(v.toString()+"''");
				v2.add(i, w);
				network.add( w );
				network.addEdge( v1.get(i), v2.get(i), 1.0 );
			} catch (Exception e) { e.printStackTrace(); }
		}

		// generate edges
		try {
			for (Edge e : graph.getAllEdges()) {
				Vertex v = e.getVertexA();
				Vertex w = e.getVertexB();
				i = vertices.indexOf( v );
				j = vertices.indexOf( w );
				network.addEdge( v2.get(i), v1.get(j), 1.0 );
				network.addEdge( v2.get(j), v1.get(i), 1.0 );
			}
		} catch (Exception e) { e.printStackTrace(); }
	}
}
