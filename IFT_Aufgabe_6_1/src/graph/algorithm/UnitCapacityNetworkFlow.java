package graph.algorithm;

import java.util.*;

import graph.*;

/**
 * Class that provides a flow on a 0-1-<tt>Network</tt>, i.e. a network with
 * each of its edges having a flow of either 0 or 1.
 * The capacity (i.e. the weight) of an edge is interpreted as 0 if it is 0
 * and interpreted as 1 otherwise. Having it this way we need not define
 * a new class for unit capacity networks, but can use the regular
 * <tt>Network</tt> classes here as well.
 * The class provides an efficient algorithm derived from that of Dinic for
 * finding a maximum flow.
 *
 * @author Ralf Vandenhouten
 * @version 2.0 2010-09-15
 */

public class UnitCapacityNetworkFlow extends NetworkFlow {

	private static final long serialVersionUID = 2L;

	/**
	 * Number of vertices in the network.
	 */
	int nvertices;

	/**
	 * Indices of source and sink vertex, respectively.
	 */
	int sourceIndex, sinkIndex;

	/**
	 * Array storing the vertices linearly.
	 */
	Vertex[] vertex;

	/**
	 * HashMap storing the indices of the vertices in the above array vertex.
	 */
	HashMap<Vertex,Integer> vertIndex;

	ArrayList<Vertex>[] successors;

	/**
	 * Arrays of lists containing the adjacent vertices.
	 */
	ArrayList<Vertex>[] residueSuccessors;

	/**
	 * Flow matrix storing the binary flow of the edges.
	 */
	boolean[][] flow;

	/**
	 * Constructor for instances of this class.
	 */
	@SuppressWarnings("unchecked")
	public UnitCapacityNetworkFlow( Network network ) throws NetworkException {
		super( network );
		int i = 0, j;
		nvertices = network.getVerticesCount();
		vertex = new Vertex[nvertices];
		vertIndex = new HashMap<Vertex,Integer>(nvertices*3/2);
		successors = (ArrayList<Vertex>[]) new ArrayList<?>[nvertices];
		residueSuccessors = (ArrayList<Vertex>[]) new ArrayList<?>[nvertices];
		flow = new boolean[nvertices][nvertices];

		// Initializing the data structures
		for (Iterator<Vertex> it = network.getVerticesIterator(); it.hasNext(); ) {
			Vertex v = it.next();
			vertIndex.put( v, new Integer(i) );
			successors[i] = 
				(ArrayList<Vertex>)((ArrayList<?>)network.getOutgoingAdjacentVertices(v)).clone();
			vertex[i++] = v;
		}
		for (i=0; i<nvertices; i++) {
			for (j=0; j<nvertices; j++) {
				flow[i][j] = false;
			}
		}
		try {
			sourceIndex = ((Integer)vertIndex.get( source )).intValue();
			sinkIndex = ((Integer)vertIndex.get( sink )).intValue();
		} catch (Exception e) {
			throw new NetworkException("Source or sink not defined.");
		}
	}

	/**
	 * Method for finding a maximum flow in the network. The method uses the
	 * algorithm of Dinic by iteratively searching blocking flows in
	 * breadth-first search-tree residue networks.
	 */
	public void maximizeFlow() {
		while ( residueNetwork() )
			blockingFlow();

		// Finally generate the flow HashMap
		for (Edge e : network.getAllEdges()) {
			int i = vertIndex.get(e.getVertexA());
			int j = vertIndex.get(e.getVertexB());
			flowMap.put( e, flow[i][j]?1:0 );
		}
	}

	/**
	 * Compute a residue network by a breadth-first search of paths from source
	 * to sink.
	 *
	 * @return Returns true if the breadth-first search finds a path to the sink,
	 *         otherwise false.
	 */
	public boolean residueNetwork() {
		int i, index;
		Queue<Integer> q = new ArrayDeque<Integer>();
		boolean[] visited = new boolean[nvertices];
		int[] niveau = new int[nvertices];
		int currentNiveau=0, stopNiveau=nvertices-1;
		boolean found = false;

		// Initializing
		for (i=0; i<nvertices; i++) {
			residueSuccessors[i] = new ArrayList<Vertex>();
		}

		// breadth-first search through the network until sink is found
		q.add(sourceIndex);
		visited[sourceIndex] = true;
		while ( !q.isEmpty() ) {
			try {
				index = ((Integer)q.remove()).intValue();
			} catch (Exception e) { e.printStackTrace(); return false; }
			currentNiveau = niveau[index]+1;
			if ( currentNiveau > stopNiveau )
				break;

			for (Vertex v : (Iterable<Vertex>)successors[index]) {
				int neighbor = vertIndex.get(v);
				if ( visited[neighbor] && niveau[neighbor] < currentNiveau )
					continue;
				residueSuccessors[index].add(vertex[neighbor]);
				if ( !visited[neighbor] && currentNiveau < stopNiveau )
					q.add(neighbor);
				if ( neighbor == sinkIndex ) {
					stopNiveau = currentNiveau;
					found = true;
				}
				visited[neighbor] = true;
				niveau[neighbor] = currentNiveau;
			}
		}
		return found;
	}

	/**
	 * Compute a blocking flow by using findFlowPath() iteratively.
	 */
	public void blockingFlow() {
		while ( findFlowPath( sourceIndex ) )
			; // this is not a mistake!
	}

	/**
	 * Helper method for finding flow paths used by maximizeFlow().
	 */
	private boolean findFlowPath(int vertex) {
		if ( residueSuccessors[vertex].isEmpty() )
			return false;
		else
			if ( residueSuccessors[vertex].contains( sink ) ) { // sink found
				flow[vertex][sinkIndex] = true;
				successors[vertex].remove( sink );
				residueSuccessors[vertex].remove( sink );
				return true;
			} else { // depth-first-search for a path to sink
				int size;
				while ( (size = residueSuccessors[vertex].size()) > 0 ) {
					Vertex vj = (Vertex)
					residueSuccessors[vertex].remove(size-1);
					// Recursive call to findFlowPath()
					int j = ((Integer)vertIndex.get(vj)).intValue();
					if ( findFlowPath(j) ) {
						flow[vertex][j] = true;
						// Remove edge from the original graph
						successors[vertex].remove( vj );
						return true;
					}
				}
			}
		return false;
	}
}