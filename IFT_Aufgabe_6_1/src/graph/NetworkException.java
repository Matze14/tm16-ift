package graph;

public class NetworkException extends GraphException {

	private static final long serialVersionUID = 1L;

	public NetworkException() {
        super();
    }

    public NetworkException( String msg ) {
        super( msg );
    }
}