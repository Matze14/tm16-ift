package graph;

public class NegativeCapacityException extends GraphException {

	private static final long serialVersionUID = 1L;

	public NegativeCapacityException() {
        super();
    }

    public NegativeCapacityException( String msg ) {
        super( msg );
    }
}