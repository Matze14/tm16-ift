package graph;

import java.util.Iterator;
import java.util.List;

/**
 * Diese Klasse enthält eine Implementierung des Algorithmus von Warshall und
 * Roy.
 * 
 * @author Matthias Graffenberger, Max Dallüge
 */
public class TransitiverAbschluss {

	/**
	 * Diese Methode ist die Implementierung des Algorithmus von Warshall und
	 * Roy. Er übernimmt ein Graphen-Objekt und liefert diesen in form eines
	 * transitiven Abschlusses wieder zurück.
	 * 
	 * @param l_graph
	 *            der Graph, dessen transitiver Abschluss ermittelt werden soll
	 * @return den Graph mit transitiven Abschluss
	 * @throws Exception
	 */
	public static Graph transAbschluss(Graph l_graph) throws Exception {
		// es wird eine Liste aller Vertex-Objekte gespeichert
		List<Vertex> l_allVertices = l_graph.getVertices();

		// der erste Iterator wird erzeugt
		Iterator<Vertex> l_l = l_allVertices.iterator();
		while (l_l.hasNext()) {
			// Vertex B wird ermittelt
			Vertex l_vertexB = l_l.next();
			/*
			 * An dieser Stelle wird wieder ein neuer Iterator erstellt, weil
			 * für jeden Iteratordurchlauf von l_l der Iterator l_i durchlaufen
			 * werden muss.
			 */
			Iterator<Vertex> l_i = l_allVertices.iterator();
			while (l_i.hasNext()) {
				Vertex l_vertexA = l_i.next();
				// wenn eine Verbindung zwischen Vertex A und B besteht
				if (l_graph.haveCommonEdge(l_vertexA, l_vertexB)) {
					/*
					 * An dieser Stelle wird wieder ein neuer Iterator erstellt,
					 * weil für jeden Iteratordurchlauf von l_i der Iterator l_j
					 * durchlaufen werden muss.
					 */
					Iterator<Vertex> l_j = l_allVertices.iterator();
					while (l_j.hasNext()) {
						Vertex l_vertexC = l_j.next();
						// wenn eine Verbindung zwischen Vertex B und C besteht
						// wenn keine Verbindung zwischen Vertex A und C besteht
						if (l_graph.haveCommonEdge(l_vertexB, l_vertexC)
								&& !l_graph.haveCommonEdge(l_vertexA, l_vertexC)) {
							l_graph.addEdge(new Edge(l_vertexA, l_vertexC));
							;
						}
					}
				}
			}
		}
		return l_graph;
	}
}
