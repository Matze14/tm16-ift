package graph;

import java.util.Iterator;
import java.util.List;

public class TransitiverAbschlussMix {

	public static Graph transAbschluss(Graph l_graph) throws Exception {
		List<Vertex> l_allVertices = l_graph.getVertices();
		Iterator<Vertex> l_l = l_allVertices.iterator();
		Iterator<Vertex> l_i = l_allVertices.iterator();
		Iterator<Vertex> l_j = l_allVertices.iterator();

		for (l_l = l_allVertices.iterator(); l_l.hasNext();) {
			Vertex l_vertexB = l_l.next();
			for (l_i = l_allVertices.iterator(); l_i.hasNext();) {
				Vertex l_vertexA = l_i.next();
				if (l_graph.haveCommonEdge(l_vertexA, l_vertexB)) {
					for (l_j = l_allVertices.iterator(); l_j.hasNext();) {
						Vertex l_vertexC = l_j.next();
						if (l_graph.haveCommonEdge(l_vertexB, l_vertexC)
								&& !l_graph.haveCommonEdge(l_vertexA, l_vertexC)) {
							l_graph.addEdge(new Edge(l_vertexA, l_vertexC));
							;
						}
					}
				}
			}
		}
		return l_graph;
	}

	public static Graph transAbschluss2(Graph l_graph) throws Exception {
		List<Vertex> l_allVertices = l_graph.getVertices();

		for (int l = 0; l < l_allVertices.size(); l++) {
			Vertex l_vertexB = l_allVertices.get(l);
			for (int i = 0; i < l_allVertices.size(); i++) {
				Vertex l_vertexA = l_allVertices.get(i);
				if (l_graph.haveCommonEdge(l_vertexA, l_vertexB)) {
					for (int j = 0; j < l_allVertices.size(); j++) {
						Vertex l_vertexC = l_allVertices.get(j);
						if (l_graph.haveCommonEdge(l_vertexB, l_vertexC)
								&& !l_graph.haveCommonEdge(l_vertexA, l_vertexC)) {
							l_graph.addEdge(new Edge(l_vertexA, l_vertexC));
							;
						}
					}
				}
			}
		}
		return l_graph;
	}
	
	public static Graph transAbschluss3(Graph l_graph) throws Exception {
		List<Vertex> l_allVertices = l_graph.getVertices();
		
		Iterator<Vertex> l_l = l_allVertices.iterator();
		while(l_l.hasNext()) {
			Vertex l_vertexB = l_l.next();
			Iterator<Vertex> l_i = l_allVertices.iterator();
			while(l_i.hasNext()) {
				Vertex l_vertexA = l_i.next();
				if (l_graph.haveCommonEdge(l_vertexA, l_vertexB)) {
					Iterator<Vertex> l_j = l_allVertices.iterator();
					while(l_j.hasNext()) {
						Vertex l_vertexC = l_j.next();
						if (l_graph.haveCommonEdge(l_vertexB, l_vertexC)
								&& !l_graph.haveCommonEdge(l_vertexA, l_vertexC)) {
							l_graph.addEdge(new Edge(l_vertexA, l_vertexC));
							;
						}
					}
				}
			}
		}
		return l_graph;
	}
}
