package userinterface;

import graph.*;

/**
 * 
 * @author Prof. Dr. rer. nat. Vandenhouten, Ralf Raimund
 * @edited_by Matthias Graffenberger und Max Dallüge
 *
 */
public class TestGraph {
	Vertex v1, v2, v3, v4, v5, v6;
	Graph graph;

	public TestGraph() throws Exception {
		// dies ist der Graph aus Aufgabe 3.1
		graph = new GraphAdMatrix(true);

		// Ecken erzeugen
		v1 = new Vertex("1");
		v2 = new Vertex("2");
		v3 = new Vertex("3");
		v4 = new Vertex("4");
		v5 = new Vertex("5");
		v6 = new Vertex("6");

		// Ecken dem Graphen hinzufuegen
		graph.add(v1);
		graph.add(v2);
		graph.add(v3);
		graph.add(v4);
		graph.add(v5);
		graph.add(v6);

		// Kanten erzeugen und hinzufuegen
		graph.addEdge(v1, v6);
		graph.addEdge(v2, v1);
		graph.addEdge(v2, v3);
		graph.addEdge(v2, v4);
		graph.addEdge(v3, v4);
		graph.addEdge(v3, v5);
		graph.addEdge(v5, v4);
		graph.addEdge(v5, v6);

		TransitiverAbschluss.transAbschluss(graph);
		System.out.println(graph);
	}

	public static void main(String[] args) throws Exception {
		@SuppressWarnings("unused")
		TestGraph test = new TestGraph();
	}
}
