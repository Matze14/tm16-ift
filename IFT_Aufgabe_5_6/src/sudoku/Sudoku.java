package sudoku;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import graph.Vertex;
import graph.algorithm.BackTrackingWithInitialGamefield;
import graph.algorithm.GraphColoring;
import graph.algorithm.NotEnoughColorsException;

/**
 * Diese Klasse stellt eine Datenstruktur für die Verwaltung und Lösung des
 * Sudokus bereit.
 * 
 * @author Matthias Graffenberger und Max Dallüge
 */
public class Sudoku {

	// enthält die Werte aus der Datei
	private HashMap<Vertex, Integer> initialGameField = null;
	// Objekt zum lösen des Sudokus
	private SudokuSolver solver = null;
	//Größe eines Sudokus (immer gleich)
	private static final int SIZE_OF_SUDOKU = 9;

	/**
	 * Diese Konstruktor erstellt nach Aufruf einen Graphen, welcher
	 * das Spielfeld beinhaltet und ein Objekt, welches zum lösen 
	 * eines beliebigen Sudokus verwendet wird.
	 */
	public Sudoku() {
		this.initialGameField = new HashMap<Vertex, Integer>();
		this.solver = new SudokuSolver();
	}

	/**
	 * Diese Methode liest ein neues Sudoku ein und überführt die bereits
	 * gesetzten Felder in eine HashMap, welche die bereits gültigen
	 * Färbungen beinhaltet.
	 * 
	 * @param Name der Datei, welche das zu lösende Sudoku beinhaltet.
	 */
	public void readSudokuToSolve(String filename) {
		try {
			// Einlesen der Datei
			File file = new File("src/" + filename);
			@SuppressWarnings("resource")
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

			// Datei wird in eine HashMap überführt
			for (int i = 0; i < SIZE_OF_SUDOKU; i++) {
				String row = reader.readLine();
				for (int j = 0; j < SIZE_OF_SUDOKU; j++) {
					// prüfen, ob der eingelesene Char eine Zahl ist
					if (Character.isDigit(row.charAt(j))) {
						// Zahl der HashMap hinzufügen (wenn es keine Null ist)
						int vertexPosition = (i * 9) + j;
						int color = Integer.parseInt(row.substring(j, j + 1));
						if (color != 0) {
							// Wenn Färbung keine Null ist
							this.initialGameField.put(solver.getCompleteGraph().getVertices().get(vertexPosition),
									color);
						}
					}
				}
			}
			// Ausgabe des Startzustandes
			System.out.println("Startzustand:");
			printActualGamefield(this.initialGameField);
		} catch (FileNotFoundException e) {
			System.out.println("Die Datei wurde nicht gefunden. Bitte ändern Sie den Dateinamen!");
		} catch (IOException e) {
			System.out.println("Beim Auslesen der Datei ist ein Fehler aufgetreten.");
		}
	}

	/**
	 * Diese Methode startet den Lösungsvorgang und gibt die Lösung aus, falls
	 * eine vorhanden ist.
	 */
	public void solveSudoku() {
		// solving the sudoku with backtracking
		System.out.println("Lösung:");
		GraphColoring solvingSudokuBackTrack = new BackTrackingWithInitialGamefield(this.solver.getCompleteGraph(), this.initialGameField);
		try {
			Map<Vertex, Integer> resultSudoku = solvingSudokuBackTrack.coloring(9);
			printActualGamefield(resultSudoku);
		} catch (NotEnoughColorsException e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * Diese Methode nimmt eine formatierte Ausgabe des Sudokufeldes vor.
	 */
	public void printActualGamefield(Map<Vertex, Integer> set) {
		System.out.println("-------------------------------");
		for (int i = 0; i < SIZE_OF_SUDOKU; i++) {
			for (int j = 0; j < SIZE_OF_SUDOKU; j++) {
				if (j % 3 == 0) {
					System.out.print("|");
				}
				int nextVertex = (i * 9) + j;
				Integer nextColor = set.get(this.solver.getCompleteGraph().getVertices().get(nextVertex));
				if (nextColor == null) {
					System.out.print(" " + 0 + " ");
				} else {
					System.out.print(" " + nextColor + " ");
				}
			}
			System.out.println("|");
			if (i % 3 == 2) {
				System.out.println("-------------------------------");
			}
		}
	}
}
