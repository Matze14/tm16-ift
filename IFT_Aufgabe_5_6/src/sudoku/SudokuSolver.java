package sudoku;

import graph.Graph;
import graph.GraphImpl;
import graph.Vertex;

/**
 * Diese Methode beinhaltet alle nötigen Methoden, um den Graphen für das Lösen
 * des Sudokus zu erstellen.
 * 
 * @author Matthias Graffenberger und Max Dallüge
 *
 */
public class SudokuSolver {

	// Graph in dem alle Werte gespeichert werden
	private Graph completeGraph = null;
	// feste Größe des Sudokufeldes
	private static final int SIZE_OF_SUDOKU = 9;

	/**
	 * Dieser Konstruktor erstellt einen ungerichteten Graphen und befüllt ihn
	 * mit allen nötigen Ecken und Kanten.
	 */
	public SudokuSolver() {
		this.completeGraph = new GraphImpl(false);
		this.createAllIsolatedVerteces();
		this.addEdgesOnHorizontal();
		this.addEdgesOnVertical();
		this.addEdgesOn3x3Field();
	}

	/**
	 * Diese Methode erstellt 81 (9x9 Felder) isolierte Knoten in dem Graphen.
	 */
	private void createAllIsolatedVerteces() {
		for (int i = 0; i < (SIZE_OF_SUDOKU * SIZE_OF_SUDOKU); i++) {
			try {
				String number = String.valueOf(i);
				Vertex v = new Vertex(number);
				v.setString(number);
				this.completeGraph.add(v);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Deise Methode erstellt Kanten zwischen jeweils einem und allen anderen
	 * Knoten in einer Zeile des Sudokus, da diese miteinander verbunden sein
	 * müssen (Nachbarn) um nicht die gleiche Färbung (Zahlen 1-9) zu erhalten.
	 */
	private void addEdgesOnHorizontal() {
		for (int i = 0; i < SIZE_OF_SUDOKU; i++) {
			for (int j = 0; j < SIZE_OF_SUDOKU; j++) {
				Vertex a = this.completeGraph.getVertices().get(j + (i * 9));
				for (int k = 0; k < SIZE_OF_SUDOKU; k++) {
					Vertex x = this.completeGraph.getVertices().get(k + (i * 9));
					if ((a != x) && (!this.completeGraph.haveCommonEdge(a, x))) {
						try {
							this.completeGraph.addEdge(a, x);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Deise Methode erstellt Kanten zwischen jeweils einem und allen anderen
	 * Knoten in einer Spalte des Sudokus, da diese miteinander verbunden sein
	 * müssen (Nachbarn) um nicht die gleiche Färbung (Zahlen 1-9) zu erhalten.
	 */
	private void addEdgesOnVertical() {
		for (int i = 0; i < SIZE_OF_SUDOKU; i++) {
			for (int j = 0; j < SIZE_OF_SUDOKU; j++) {
				Vertex a = this.completeGraph.getVertices().get(i + (j * 9));
				for (int k = 0; k < SIZE_OF_SUDOKU; k++) {
					Vertex x = this.completeGraph.getVertices().get(i + (k * 9));
					if ((a != x) && (!this.completeGraph.haveCommonEdge(a, x))) {
						try {
							this.completeGraph.addEdge(a, x);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Deise Methode erstellt Kanten zwischen jeweils einem und allen anderen
	 * Knoten in einem 3x3-Feld des Sudokus, da diese miteinander verbunden sein
	 * müssen (Nachbarn) um nicht die gleiche Färbung (Zahlen 1-9) zu erhalten.
	 */
	private void addEdgesOn3x3Field() {
		for (int i = 0; i < SIZE_OF_SUDOKU; i++) {
			int helper = i;
			if (i >= 6) {
				helper = i + 12;
			} else if (i >= 3) {
				helper = i + 6;
			}

			for (int j = 0; j < SIZE_OF_SUDOKU; j++) {
				int jIndex = 0;
				if (j <= 2) {
					jIndex = (helper * 3) + j;
				} else if (j <= 5) {
					jIndex = (helper * 3) + j + 6;
				} else if (j <= 8) {
					jIndex = (helper * 3) + j + 12;
				}

				Vertex a = this.completeGraph.getVertices().get(jIndex);

				/*
				 * In dieser For-Schleife werden alle Felder aus einem
				 * 3x3 mit Kanten bestückt.
				 */
				for (int k = 0; k < SIZE_OF_SUDOKU; k++) {
					int kIndex = 0;
					if (k <= 2) {
						//erste Zeile des 3x3-Feldes
						kIndex = (helper * 3) + k;
					} else if (k <= 5) {
						//zweite Zeile des 3x3-Feldes
						kIndex = (helper * 3) + k + 6;
					} else if (k <= 8) {
						//dritte Zeile des 3x3-Feldes
						kIndex = (helper * 3) + k + 12;
					}

					Vertex x = this.completeGraph.getVertices().get(kIndex);
					if (!this.completeGraph.haveCommonEdge(a, x) && (a != x)) {
						try {
							this.completeGraph.addEdge(a, x);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Diese Methode liefert den kompletten Graphen.
	 * 
	 * @return aktuelle Graph
	 */
	public Graph getCompleteGraph() {
		return completeGraph;
	}
}
