import sudoku.Sudoku;

/**
 * 
 * Diese Klasse startet die Main um den Quellcode zu testen.
 * 
 * @author Matthias Graffenberger und Max Dallüge
 *
 */
public class TestUI {

	public static void main(String[] args) {
		Sudoku sudoku = new Sudoku();
		sudoku.readSudokuToSolve("Sudoku1.txt");
		sudoku.solveSudoku();
	}

}
