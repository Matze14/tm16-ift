package graph.algorithm;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import graph.Graph;
import graph.Vertex;

/**
 * Diese Klasse ermöglicht die Verwendung des Backtracking-Algorithmus mit einem
 * Startspielfeld.
 * 
 * @author Matthias Graffenberger und Max Dallüge
 * 
 */
public class BackTrackingWithInitialGamefield extends BackTrackColoring {

	private static final long serialVersionUID = 1L;

	// die Liste aller sortierten Graphen
	List<Vertex> sortedVertices;

	// startIndex an dem das letzte Element bereits eine Färbung besitzt
	Integer startIndexOfSortedList = 0;

	/**
	 * Der Konstruktor, der die Übergabe eines initialen Spielfledes ermöglicht.
	 *
	 * @param der Graph, der alle Spielregelen beinhaltet
	 * @param das initiale Spielfeld, welches die Daten aus der Datei beinhaltet
	 */
	public BackTrackingWithInitialGamefield(Graph gameField, HashMap<Vertex, Integer> initialGameField) {
		super(gameField);

		/*
		 * Die Liste mit den "Lösungen" beginnt beim letzten Vektor mit einer
		 * vorhandenen Farbe.
		 */
		startIndexOfSortedList = initialGameField.size();

		/*
		 * Setzt die colorMap aus der Superklasse gleich der Map die diesem
		 * Konstruktor übergeben wird.
		 */
		colorMap = initialGameField;

		/*
		 * Hier wird die Liste aller Knoten im Graphen verglichen mit allen
		 * Knoten aus der HashMap. Die Liste der Knoten aus dem Graphen wird
		 * dabei so sortiert, das am Anfang alle Knoten stehen, die bereits eine
		 * Lösung durch das vorgegebene Rätsel haben (HashMap). Durch die
		 * Übergabe des Startindex, welcher der Anzahl der bereits vorhandenen
		 * Lösungen (in der HashMap) entspricht, werden die bereits vorhandenen
		 * Werte der HashMap nicht gelöscht bzw. überschrieben und es wird nach
		 * den bereits gefärbten Felder des Graphen angefangen zu suchen.
		 */
		sortedVertices = graph.cloneVertices();
		sortedVertices.sort(new Comparator<Vertex>() {
			public int compare(Vertex o1, Vertex o2) {
				Vertex a = (Vertex) o1;
				Vertex b = (Vertex) o2;

				Integer value1 = null;
				Integer value2 = null;

				/*
				 * Hier wird verglichen, ob Vertex a schon eine Färbung hat.
				 */
				if (colorMap.containsKey(a)) {
					value1 = 0;
				} else {
					value1 = 1;
				}

				/*
				 * Hier wird verglichen, ob Vertex b schon eine Färbung hat.
				 */
				if (colorMap.containsKey(b)) {
					value2 = 0;
				} else {
					value2 = 1;
				}

				if (value1 < value2) {
					return -1;
				} else if (value1 > value2) {
					return 1;
				} else {
					return 0;
				}
			}

			public boolean equals(Object obj) {
				return obj.equals(this);
			}

		});

		vertices = sortedVertices;

	}

	/**
	 * HINWEIS: Diese Methode ist aus der Klasse BackTrackColoring.java!
	 * 			Es wurde lediglich die kommentierte Zeile geändert.
	 * 
	 * Coloring method using a back tracking algorithm.
	 *
	 * @param maxNumOfColors
	 *            The maximum number of colors to be used for coloring. If that
	 *            is not enough, a NotEnoughColorsException is thrown.
	 *
	 * @return The HashMap containing the color mapping of the vertices.
	 * 
	 */
	public Map<Vertex, Integer> coloring(int maxNumOfColors) throws NotEnoughColorsException {
		// Aufruf der tryColor()-Funktion der Oberklasse
		// Ergänzung um den ermittelten Startindex
		if (!super.tryColor(startIndexOfSortedList, maxNumOfColors))
			throw new NotEnoughColorsException("Backtracking needs more than " + maxNumOfColors + " colors.");
		else
			return colorMap;
	}
}